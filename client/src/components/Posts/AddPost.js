import React, { Component } from "react";
import PostDataService from "../../services/PostDataService";

export default class AddPost extends Component {
  constructor(props) {
    super(props);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeContent = this.onChangeContent.bind(this);
    this.onChangePublished = this.onChangePublished.bind(this);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.savePost = this.savePost.bind(this);
    this.newPost = this.newPost.bind(this);

    this.state = {
      id: null,
      title: "",
      content: "",
      username: "",
      published: false,
      submitted: false
    };
  }

  onChangeTitle(e) {
    this.setState({
      title: e.target.value
    });
  }

  onChangeContent(e) {
    this.setState({
      content: e.target.value
    });
  }

  onChangeUsername(e) {
    this.setState({
      username: e.target.value
    });
  }

  onChangePublished(e) {
    this.setState({
      published: e.target.checked
    });
  }

  savePost() {
    var data = {
      title: this.state.title,
      content: this.state.content,
      username: this.state.username,
      published: this.state.published
    };

    PostDataService.create(data)
      .then(response => {
        this.setState({
          id: response.data.id,
          title: response.data.title,
          content: response.data.content,
          username: response.data.username,
          published: response.data.published,
          submitted: true
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  newPost() {
    this.setState({
      id: null,
      title: "",
      content: "",
      published: false,

      submitted: false
    });
  }

  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>You submitted successfully!</h4>
            <button className="btn btn-success" onClick={this.newPost}>
              Add more
            </button>
          </div>
        ) : (
          <div>
            <div className="form-group">
              <label htmlFor="title">Title</label>
              <input
                type="text"
                className="form-control"
                id="title"
                required
                value={this.state.title}
                onChange={this.onChangeTitle}
                name="title"
              />
            </div>

            <div className="form-group">
              <label htmlFor="content">Content</label>
              <textarea
                className="form-control" aria-label="Content" id="content"
                value={this.state.content}
                onChange={this.onChangeContent}
                required
              ></textarea>
            </div>

            <div className="input-group flex-nowrap">
              <span className="input-group-text" id="addon-wrapping">@</span>
              <input
                type="text"
                className="form-control"
                placeholder="Username"
                aria-label="Username"
                aria-describedby="addon-wrapping"
                value={this.state.username}
                onChange={this.onChangeUsername}
              />
            </div>

            <div className="form-group">
              <input
                className="form-check-input"
                type="checkbox"
                value={this.state.published}
                id="published"
                onChange={this.onChangePublished}
              />
              <label className="form-check-label">
                 Published
              </label>
            </div>

            <button onClick={this.savePost} className="btn btn-success">
              Submit
            </button>
          </div>
        )}
      </div>
    );
  }
}

import React, { Component } from "react";
import PostDataService from "../../services/PostDataService";

export default class Post extends Component {
  constructor(props) {
    super(props);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeContent = this.onChangeContent.bind(this);
    this.getPost = this.getPost.bind(this);
    this.updatePublished = this.updatePublished.bind(this);
    this.updatePost = this.updatePost.bind(this);
    this.deletePost = this.deletePost.bind(this);

    this.state = {
      currentPost: {
        id: null,
        title: "",
        content: "",
        published: false
      },
      message: ""
    };
  }

  componentDidMount() {
    this.getPost(this.props.match.params.id);
  }

  onChangeTitle(e) {
    const title = e.target.value;

    this.setState(function(prevState) {
      return {
        currentPost: {
          ...prevState.currentPost,
          title: title
        }
      };
    });
  }

  onChangeContent(e) {
    const content = e.target.value;
    
    this.setState(prevState => ({
      currentPost: {
        ...prevState.currentPost,
        content: content
      }
    }));
  }

  getPost(id) {
    PostDataService.get(id)
      .then(response => {
        this.setState({
          currentPost: {
            ...response.data,
            published: response.data.published == "1"
          }
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  updatePublished(status) {
    var data = {
      id: this.state.currentPost.id,
      title: this.state.currentPost.title,
      content: this.state.currentPost.content,
      published: status
    };

    PostDataService.update(this.state.currentPost.id, data)
      .then(response => {
        this.setState(prevState => ({
          currentPost: {
            ...prevState.currentPost,
            published: status
          }
        }));
      })
      .catch(e => {
        console.log(e);
      });
  }

  updatePost() {
    PostDataService.update(
      this.state.currentPost.id,
      this.state.currentPost
    )
      .then(response => {
        this.setState({
          message: "The post was updated successfully!"
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  deletePost() {    
    PostDataService.delete(this.state.currentPost.id)
      .then(response => {
        this.props.history.push('/home')
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { currentPost } = this.state;

    return (
      <div>
        {currentPost ? (
          <div className="edit-form">
            <h4>Post</h4>
            <form>
              <div className="form-group">
                <label htmlFor="title">Title</label>
                <input
                  type="text"
                  className="form-control"
                  id="title"
                  value={currentPost.title}
                  onChange={this.onChangeTitle}
                />
              </div>
              <div className="form-group">
                <label htmlFor="content">content</label>
                <input
                  type="text"
                  className="form-control"
                  id="content"
                  value={currentPost.content}
                  onChange={this.onChangeContent}
                />
              </div>

              <div className="form-group">
                <label>
                  <strong>Status:</strong>
                </label>
                {currentPost.published ? "Published" : "Pending"}
              </div>
            </form>

            {currentPost.published ? (
              <button
                className="badge bg-secondary"
                onClick={() => this.updatePublished(false)}
              >
                UnPublish
              </button>
            ) : (
              <button
                className="badge bg-success"
                onClick={() => this.updatePublished(true)}
              >
                Publish
              </button>
            )}

            <button
              className="badge bg-danger"
              onClick={this.deletePost}
            >
              Delete
            </button>

            <button
              type="submit"
              className="badge bg-primary"
              onClick={this.updatePost}
            >
              Update
            </button>
            <p>{this.state.message}</p>
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on a Post...</p>
          </div>
        )}
      </div>
    );
  }
}

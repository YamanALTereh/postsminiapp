import React, { Component } from 'react';

import { BrowserRouter as Router, Switch } from 'react-router-dom';
import { Route } from 'react-router';

import Home from './components/Home';
import Header from './components/Header';
import Footer from './components/Footer';
import About from './components/About';
import Contact from './components/Contact';
import NotFound from './components/NotFound';
import AddPost from './components/Posts/AddPost'
import Post from './components/Posts/Post'
import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <Header/>

        <main className="flex-shrink-0">
          <div className="container">
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/about" component={About} />
              <Route path="/contact" component={Contact} />
              <Route path="/posts/new" component={AddPost} />
              <Route path="/posts/:id" component={Post} />
              <Route component={NotFound} />
            </Switch>
          </div>
        </main>

        <Footer/>
      </Router>
    )
  }
}

export default App;
CREATE TABLE IF NOT EXISTS posts (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username INTEGER,
  title TEXT,
  content TEXT,
  published TEXT,
  createdAt TEXT,
  updatedAt TEXT
);

const db = require("../models");
const Post = db.posts;
const Op = db.Sequelize.Op;
// TODO, use serialization for response data
// TODO, Implement pagiantion for post list api

// Create and Save a new post
exports.create = (req, res) => {
  // Validate request
  if (!req.body.title) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a post
  const post = {
    title: req.body.title,
    content: req.body.content,
    username: req.body.username,
    published: req.body.published ? req.body.published : false
  };

  // Save post in the database
  Post.create(post)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Post."
      });
    });
};

// Post all posts from the database.
exports.findAll = (req, res) => {
  console.log("req: ", req)
  const title = req.query.title;
  var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  Post.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving posts."
      });
    });
};

// Find a single post with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Post.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving post with id=" + id
      });
    });
};

// Update a post by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Post.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "post was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update post with id=${id}`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error with updating post with id=" + id
      });
    });
};

// Delete a post with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Post.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "post was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete post with id=${id}.`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: `Could not delete post with id=${id}.`
      });
    });
};

// Delete all posts from the database.
exports.deleteAll = (req, res) => {
  Post.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} posts were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Something went wrong."
      });
    });
};

// find all published post
exports.findAllPublished = (req, res) => {
  Post.findAll({ where: { published: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Something went wrong."
      });
    });
};
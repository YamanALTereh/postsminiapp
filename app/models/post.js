module.exports = (sequelize, Sequelize) => {
  const Post = sequelize.define("posts", {
    username: {
      type: Sequelize.STRING
    },
    title: {
      type: Sequelize.STRING
    },
    content: {
      type: Sequelize.STRING
    },
    published: {
      type: Sequelize.BOOLEAN
    }
  });

  return Post;
};

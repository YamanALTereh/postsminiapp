var express = require('express');
var router = express.Router();

module.exports = function(app){
  app.use('/api/v1', router);

  router.get('/posts', function(req, res) {
    res.json({ posts: [{id: 1, title: 'title 1'}]});
  });

  //other routes..
}

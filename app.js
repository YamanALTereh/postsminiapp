var express = require('express')
  , http = require('http')
  , path = require('path')
  , bodyParser = require('body-parser')
  , favicon = require('serve-favicon')
  , logger = require('morgan')
  , methodOverride = require('method-override')
  , cors = require('cors');

var app = express();

app.set('port', process.env.PORT || 4000);
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(methodOverride('_method'));
app.use(express.static(path.join(__dirname, 'client/build')));
app.use(cors())

require('./app/routes/posts')(app);

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
app.get('/alive', function(req, res) {
  res.json({ message: 'welcome to our api!' });
});

if (app.get('env') == 'development') {
  app.locals.pretty = true;
}

const db = require("./app/models");

// The "catchall" handler: for any request that doesn't
// match one above, send back React's index.html file.
// app.get('*', (req, res) => {
//   res.sendFile(path.join(__dirname+'/client/build/index.html'));
// });

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Dependencies
* Setup and run
* Screenshots
* Backend Apis spec

### Quick summary ###
- Using Express framework for backend application.
- Using ReactJs with bootstrap for client application.
- Using Sequelize for ORM.
- Using Sqlite3 for database.
- The repo includes `client` folder which is the reactJs client app.
- The `app` folder is for backend express app.
- Using file `conig/db.config.js` for database configs.
- Following MVC pattern for backend app.
- Following restful for backend APIs.

### Dependceies ###
- Node version `v12.16.2`
- DataBase Sqlite3

### Setup and run ###
- clone the repo
- Create DB based on dbConfig file
- Run the `db/CreatePostTable.sql` file to create `posts` table
- Go the repo folder
```bash
cd posts-app
```

- Install backed app dependceies
```bash
npm install
```

- Run backed app
```bash
npm start
```

- Go to the client folder
```bash
cd client
```

- Install client app dependceies
```bash
npm install
```

- Run Client app in development mode
```bash
npm start
```

- Go to `http://localhost:3000/` 

- To run production mode, need to build the client app:
```bash
cd client
npm run build
cd ..
npm start
```

- Go to `http://localhost:4000/` 

### Screenshots
- Home page
![Home](https://bitbucket.org/repo/AXjzBMX/images/3608944928-Screenshot%202021-12-13%20at%2012.46.03%20PM.png)

- Create new Post
![CreatePost](https://bitbucket.org/repo/AXjzBMX/images/3854033818-Screenshot%202021-12-13%20at%2012.46.16%20PM.png)

- Edit Post
![EditPost](https://bitbucket.org/repo/AXjzBMX/images/2622815191-Screenshot%202021-12-13%20at%2012.47.08%20PM.png)

- Publish Post
![PublishPost](https://bitbucket.org/repo/AXjzBMX/images/905957069-Screenshot%202021-12-13%20at%2012.47.02%20PM.png)

### Backend Apis spec
- to fetch all posts
```bash
curl http://localhost:4000/api/v1/posts
```

- to fetch only published posts
```bash
curl http://localhost:4000/api/v1/posts/published
```

- to create new post
```bash
curl -X POST -H 'Content-Type: application/json' \
-d '{"title":"Post Title", "content": "Post long text content", "username": "uniq_username"}' \
http://localhost:4000/api/v1/posts
```

- to fetch a single post
```bash
curl -X GET -H 'Content-Type: application/json' \
http://localhost:4000/api/v1/posts/2
```

- to update a single post
```bash
curl -X PUT -H 'Content-Type: application/json' \
-d '{"title":"Updated Post Title", "content": "Updated Post long text content", "published": true}' \
http://localhost:4000/api/v1/posts/2
```

- to delete a single post
```bash
curl -X DELETE -H 'Content-Type: application/json' \
http://localhost:4000/api/v1/posts/1
```

- to delete all
```bash
curl -X DELETE -H 'Content-Type: application/json' \
http://localhost:4000/api/v1/posts
```